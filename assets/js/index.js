document.addEventListener("DOMContentLoaded", () => {
    setInterval(function () {
        esqueletos();
    }, 6000);

    function esqueletos() {
        const esqueleto = document.getElementById("esqueleto");
        if (esqueleto.className === "esqueleto-bg") {
            esqueleto.classList.remove("esqueleto-bg");
            esqueleto.classList.remove("esqueleto-3");
            esqueleto.classList.remove("esqueleto-4");

            esqueleto.classList.add("esqueleto-2");
        } else if (esqueleto.className === "esqueleto-2") {
            esqueleto.classList.remove("esqueleto-2");
            esqueleto.classList.remove("esqueleto-bg");
            esqueleto.classList.remove("esqueleto-4");

            esqueleto.classList.add("esqueleto-3");
        } else if (esqueleto.className === "esqueleto-3") {
            esqueleto.classList.remove("esqueleto-3");
            esqueleto.classList.remove("esqueleto-2");
            esqueleto.classList.remove("esqueleto-bg");

            esqueleto.classList.add("esqueleto-4");
        } else if (esqueleto.className === "esqueleto-4") {
            esqueleto.classList.remove("esqueleto-4");
            esqueleto.classList.remove("esqueleto-3");
            esqueleto.classList.remove("esqueleto-2");

            esqueleto.classList.add("esqueleto-bg");
        }
    }
});